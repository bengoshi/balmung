# Balmung

Dieses Repository enthält Material des Coder Dojo's der c-base, Berlin.

Es ist eine Einführung zur Webseiten erstellung mit Pythons Flask in Deutsch [hier](https://gitlab.com/Chaostheorie/balmung/tree/master/flask/introduction/introduction-de.md) zu finden.

Seit neuestem bieten wir [hier](https://gitlab.com/Chaostheorie/balmung/tree/master/web-theory/osi-model/introduction-de.md) eine Reihe von Artikeln zur Theorie hinter Webanwendungen und allgemeinen Grundlagen zu Computernetzwerken.

Unser vereinfachter Artikel über das OSI-Netzwerk Model ist [hier](https://gitlab.com/Chaostheorie/balmung/tree/master/web-theory/osi-model/introduction-de.md) zu finden.

&copy; Chaostheorie 2019
[![CC-BY-NC](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cc-by-nc_icon.svg/128px-Cc-by-nc_icon.svg.png)](https://creativecommons.org/licenses/by-nc/4.0/)

Alle Datein in diesem Repository, wenn nicht direkt angegeben, unterliegen dem obrigen Copyright.
Every file in the repository that doesn't show an Copyright Notice is to handle with the above Copyright Notice.

For right related issues please do not hesitate to contact us at chaos4747@gmail.com and we will response as soon as possible.
Für Rechtliche Angelegenheiten kontaktieren Sie uns bitte unter chaos4747@gmail.com und wir antworten umgehend.
