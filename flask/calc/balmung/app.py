from flask import Flask, render_template, jsonify, request
from random import randint

app = Flask(__name__)

@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(51, 100)
    return render_template("calc.html", a=a, b=b)

@app.route("/check-results")
def check_results():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    user_res = request.args.get('result', 0, type=int)
    if user_res == a+b:
        message = "Gratulation, deine Antwort ist richtig!"
    else:
        message = "Schade, deine Antwort war leider Falsch!"
    output = {"message": message}
    return jsonify(output)

if __name__ == "__main__":
    app.run(debug=True)    
