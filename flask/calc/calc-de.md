# Calculus

## Ein Kopfrechentrainer mit Flask - Schwierigkeit: 5/10 &starf;

In diesem Teil der Reihe zu Websitenerstellung mit Pythons Flask behandeln wir eine erweitertes Beispiel von einer Website die eine Mathaufgabe stellt.
Hierzu sehen wir uns asnychnrone Abfragen in Javascript mit Jquery an aber auch das bereitstellen von Daten über Flask für solche oder Ähnliche Anwendungen.

Hierfür wiederholen wir das erstellen einer Website und zeigen euche einen neuen Weg mit Flaks Websiten anzuzeigen. Auch behandeln wir das erstellen von Asnychronen Anfragen mit dem Javascript Framwoerk Jquery und wie man damit in Flask umgeht.

Zum Beginn legen wir einen neuen Ordner an. Der Einfachheit halber nennen ich meinen für diese Reihe `Balmung`.
Dann geht in den Ordner und erstellt einen weiteren Ordner mit dem Name `templates`.

<details>
<summary>Ordner Struktur</summary>
<pre>

Balmung
│
└───templates

</pre>
</details>

Als nächstes fangen wir mit dem eigentlichen Coden an und erstellen unsere `app.py`.

<details>
<summary>Ordner Struktur</summary>
<pre>

Balmung
| `app.py`
│
└───templates

</pre>
</details>

Die Datei beginen wir wie immer mit dem importieren der Module. Dieses Mal aber haben wir ein paar neue Kanidaten dabei:

```python
from flask import Flask, render_template, jsonify
from random import randint
```

Zuallerst haben wir jsonify dabei, welches es erlaubt anstatt einer Website eine Antwort im Json Format zurückzugeben.

<details>
<summary>Mehr zu Json</summary>
<pre>

Das Json (JavaScript Object Notation) zur vereinfachten Textübertragung und wurde zuerst in Javascript, welches wir später auch behandeln werden, eingeführt.

Es ist vereinfacht in Python ein dictonary. Mehr zu Python Datentypen [Hier](https://gitlab.com/Chaostheorie/tree/master/balmung/python/basics/data-types.md).

Einige nenneswerte Eigenschaften von Json sind:
    * Es ist auch ein eigener Dateitype Endung: `.json`
    * Es sind nur Strings als Keys erlaubt
    * Es gibt zum verwenden von Json ein eigenes Python Modul, welches zu den Kernmodulen gehört
    * Json ist bei der Übertragung von Daten durch die hohe Rate an Javascript Anwendungen bei z.B. Node.js, einem Serverseitigen Javascript Framework, sehr verbreitet und man kommt nicht um es herum
  
</pre>
</details>

Als nächstes haben wir das request Objekt dabie, welches ir bereits aus unserem [Artikel](https://gitlab.com/Chaostheorie/tree/master/balmung/flask/forms/introduction-de.md) zu Web Formularen kennt.

Als letztes ist noch unser alter Bekannter die `Flask` Instanz und das `randint` modul zur Generierung von zufälligen Zahlen dabei.

Lasst uns nun mit dem kreieren der Flask Instanz beginnen.

```python
app = Flask(__name__)
```

<details>
<summary>Ganzer Code</summary>
<pre>

```python
from flask import Flask, render_template, jsonify
from random import randint

app = Flask(__name__)
```

</pre>
</details>

Als nächstes erstellen wir die Start Seite mit dem einsetzten des `@app.route` decorators und dem Erstellen einer neuen Funktion:

```python
@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(1, 50)
    return render_template("calc.html", a=a, b=b)
```

<details>
<summary>Ganzer Code</summary>
<pre>

```python
from flask import Flask, render_template, jsonify
from random import randint

app = Flask(__name__)

@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(1, 50)
    return render_template("calc.html", a=a, b=b)
```

</pre>
</details>

Hierbei sehen wir das erste mal die Funktion an die an den Nutzer beim aufrufen der Url `"/"` an. Die Funktion die genutzt wird gibt wie die Parameter anzeigen eine Datei mit dem Namen `calc.html` zurück und lässt sie rendern. Als weitere Parameter geben wir noch die Variablen a und b mit, welche danach im html Template verfügbar sein werden.

Die Funktion sucht hierbei um genauer zu sein im Ordner indem sie ausgeführt wird `balmung` nach einem Ordner mit dem Namen templates und findet den vorher erstellten Ordner `balmung>templates` und sucht dort nach der Datei `calc.html`.

Damit dies funktioniert brauchen wr also einen neue Datei `calc.html` im templates Ordner anlegen.

<details>
<summary>Ordner Struktur</summary>
<pre>

```
Balmung
| app.py
│
└───templates
|   | calc.html
```

</pre>
</details>

Nun öffnen wir diese Html Datei mit dem Texteditor deines niedrigsten Misstrauens. Fals du noch nicht mit HTML ertraut bist lese den Eintrag Mehr zu Html.

<details>
<summary>Mehr zu Html</summary>
<pre>

Html (eng. Hypertext Markup Language) ist die im Web verwendete Sprach zum darstellen von Text und mit ihr wurde jede Website die du im Internet siehst gestaltet.

Um Html herum haben sich im Laufe der Jahre auch Template Enignes wie z.B. die mit Flask kommende Jinja2 Enigne aufgezeigt. Am Ende kommt es aber immer auf das Grundverständnis und den richtigen Umgang damit an.

Html arbeitet mit sogenannten Tags diese verhalten sich nach diesen Syntax `<name` und zum beenden eines Tags muss man den Namen in diesem Syntax benutzten `</name>`, z.B. beginnen eines neuen Absatztes und zum beenden danach:

```html
<p>
    Ein Interresanter Text
</p>
```

In Html können andere Sprachen wie Css, eine ähnlich große Sprache wie HTML im Internet für Styling, und Javascript, eine genauso große Sprache im Internet die Funktionsorientiert ist, also nicht um Objekte wie die Flask Instanz `Flask()` herum funktioniert sondern an Ereignisse gebundene Funktionen aufruft und sich nach diesem Pinzip aufbaut.

In unserem Fall benutzten wir die Jinja2 enigne um Variablen oder anderes aus Flask weiter zu geben und dort zu benutzten wie z.B. in unserem Fall a und b.

Mehr zu Html und wie es funktioniert [Hier](https://wiki.selfhtml.org/wiki/HTML/Tutorials/Einstieg).

</pre>
</details>

Also lasst uns mit dem erstellen der Html Datei beginnen. Zuallerst müssen wir mit dem erstellen des offensichtlichen beginnen. Den Javascript spezifischen Teil behandeln wir später mit dem jetztigen als Grundlage.

Zuerst setzten die Grundlegenden Struktur und den Titel:

```html
<! DOCTYPE html>
<html>
    <head>
        <title>Kopfrechentrainer</title>
    </head>
    <body>
        <form>
            <p>
                {{ a }} + {{ b }} =
                <input type="text" id="result">
                <input type="hidden" id="a" value="{{ a }}">
                <input type="hidden" id="b" value="{{ b }}">
                <input type="submit" value="Ergebnis absenden">
            </p>
        </form>
        <p id="message"></p>
    </body>
</html>
```

Als nächstes setzten wir ein paar versteckte Felder auf welche wir später anhand der `id` zugreifen können. Jetzt bauen wir noch ein Div Element ein um später die Antwort vom Server anzuzeigen, sowie einen `<input type="submit">` um das Formular abzusenden.

Und jetzt beginnt der richtige Spass und wir bauen den Javascript Teil mit Jquery.

Dafür fangen wir an mit dem einbinden des Jquery Frameworks, dies tun wir mit dem hinzufügen des `<script>` tags und dem angeben eines src elements, in diesem Fall benutzten wir das Google CDN als Quelle, und dem `type` der zu einbindenden Datei und den `<script>` tag für unsere Funktion.

```html
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
```

<details>
<summary>Ganzer Html Code</summary>
<pre>

```html
<! DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <title>Kopfrechentrainer</title>
        <script type="text/javascript"></script>
    </head>
    <body>
        <form>
            <p>
                {{ a }} + {{ b }} =
                <input type="text" id="result">
                <input type="hidden" id="a" value="{{ a }}">
                <input type="hidden" id="b" value="{{ b }}">
                <input type="submit" value="Ergebnis absenden">
                <button type="button" id="refresh">Neue Übung</button>
            </p>
        </form>
        <p id="message"></p>
    </body>
</html>
```

</pre>
</details>

Und jetzt schreiben wir in diesen Tag unseren Javascript Code. Wir beginnen mit dem eröffnen einer neuen Funktion durch Jquery:

```javascript
$(function () {

});
```

Und jetzt beginnt der richtige Teil, zuerst erstellen wir eine Unterfunktion, welche das absenden des Formulars abfängt un verhindert, dass es abgesendet wird:

```javascript
    $("form").submit(function (event) {
        event.preventDefault(event);
    };
```

<details>
<summary>Ganzer Javascript Code</summary>
<pre>

```javascript
$(function() {
    $("form").submit(function (event) {
        event.preventDefault(event);
    };
});
```

</pre>
</details>

Und jetzt haben wir eine Funktion die es uns erlaubt auf das benutzten des Senden Knopfes zu reagieren.
Als nächsetes steht das erhalten der Variablen a und b auf dem Plan.

Jetzt sollten wir aber mal etwas zurück gehen und uns Javascript einmal genauer ansehen.
Falls ihr Javascript schon kennt könnt ihr den unten liegenden Artikel überspringen.

<details>
<summary>Crashkurs Javascript</summary>
<pre>

Javascript ist eine Funktionsorientierte Sprache. 
Die Verbreitung von Javascript ist genauso hoch wie die von HTML und Css.
Funktionsorientiert steht hierbei dafür das man seine Programm Struckturn um das auslösen von Funktionen durch einen Event festlegt wie z.B. das abschicken eines Formulars, anstatt wie in Python eine Programmierung um ein Objekt wie die `Flask` Instanz herum.
In Javascript ist das allgemeine Verhalten von Code uch anders und verhält sich eher wie z.B. C als Sprache.

Gleichzeitig sind für uns am Anfang im Vergleich einige Sachen im Kopf zu behalten:
    * Die basischen Funktionen sidn zusammen mit vielen erweiterten Funktionen vorgeladen
    * Jeder Befehl wird mit einem `;` abgeschlossen
    * In Javascript werden Variablen definiert
    * Javascript agiert in der Reihenfolge der zu ladenden Sprachen als letzte bzw. nach Python und Jinja
    * Javascript hat andere Datentypen
    * Es wird in dieser Sprach auf Klammern gesetzt, Einrückungen sind nur zur Lesbarkeit und nicht nötig wie in Python
    * Javascript Code wird in html in einem `<script>` tag verwendet und ist sonst in `.js` Datein zu finden

Javascript kann auch in Form von Coffescript, eine Sprache welche zur besseren Verwendung erschaffen wurde, auftreten. 
Seit geraumer Zeit ist auch Serverseitiges Javascript durch das Node.js Framwork unterwegs.

Und für uns am wichtigsten: Es gibt keinen Weg um Javascript herum
</pre>
</details>

Aber jetzt zurück zum Code! Wir sollten nun weiterachen und die Parameter für die Anfrage definieren.

Diese Parameter bekommen wir durch das auslesen der `<input type="hidden">` (eng. hidden=versteckt) tags, welche als `value` (eng. Wert) die Variablen `a` und `b` haben, nicht zu vergessen das holen des vom Nutzer eingebenen Ergebnisses, welches mit der id `result` versehen ist.

Dies tun wir indem wir die Ids nehmen und den sazugehörigen Wert per javascript abgreifen. Das reicht aber noch nicht den wir brauchen auch noch ein Ziel für unsere Anfrage. Für dies nehmen wir der Einfachheit halber `/check-results`:

```javascript
    var url = "/check-results";
    var a = $("#a").val();
    var b = $("#b").val();
    var user_res = $("#result").val();
```

<details>
<summary>Ganzer Code</summary>
<pre>

```javascript
$(function () {
    $("form").submit(function (event) {
        var url = "/check-results";
        var a = $("#a").val();
        var b = $("#b").val();
        var user_res = $("#result").val();
        event.preventDefault(event);
    });
});
```

</pre>
</details>

So jetzt haben wir die Werte also kann die Anfrage beginnen, dies machen wir mit der Jquery Funktion Ajax.
Diese Funktion benötigt ein Ziel und kann darauf noch Daten, in Form eines dictonarys, dieses nenn wir hier data, mitgeben aber auch bei erfolgreicher Anfrage eine Funktion auslösen:

```javascript
var data = {
    a: a,
    b: b,
    result: user_res
    };
    $.ajax({
        dataType: "json",
        url: url,
        data: data,
        success: function (data) {
        $("#message").text(data.data.message)
        }
    });
```

<details>
<summary>Ganzer Code</summary>
<pre>

```javascript
$(function () {
    $("form").submit(function (event) {
        var url = "/check-results";
        var a = $("#a").val();
        var b = $("#b").val();
        var user_res = $("#result").val();
        var data = {
            a: a,
            b: b,
            result: user_res
        };
        $.ajax({
            dataType: "json",
            url: url,
            data: data,
            success: function (data) {
                $("#message").text(data.message)
            }
        event.preventDefault(event);
        });
});
```

</pre>
</details>

In unserem Fall setzten wir bei einer erfolgreichen Anfrage den Text des `<div id="message"></div>` Blocks zu dem zurückgegeben `data.message` wert, welchen wir gleich festlegen werden.

Damit währen wir mit dem groben Javascript Teil fertig! Den großen Stein habt ihr damit aus dem Weg geräumt. Wir werden später noch einmal für etwas Feinschlif hierauf zurückkommen aber ihr seit über den Berg.

Jezt geht es zurück zur `balmung>app.py` und wir erstellen eine neue Funktion mit dem decorator `app.route("/check-results")`:

```python
@app.route("/check-results")
def check_results():
```

<details>
<summary>Ganzer Python Code</summary>
<pre>

```python
from flask import Flask, render_template, jsonify
from random import randint

app = Flask(__name__)

@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(1, 50)
    return render_template("calc.html", a=a, b=b)

@app.route("/check-results")
def check_results():
```

</pre>
</details>

Nun könne wir damit anfangen die von Javascript aus gesendeten Daten zu empfangen und in Python zu verarbeiten, dies tun wir mit dem request Objekt und dem unter Attrbute args bzw. der Funktion `request.args.get`:

```python
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    user_res = request.args.get('user_res', 0, type=int)
```

Als nächstes vergleichen wir die Werte von oben mit dem Ergebniss von a + b und setzten passend dazu die `message`, welche auf der Website angezeigt werden soll:

```python
    if user_res == a+b:
        message = "Gratulation, deine Antwort ist richtig!"
    else:
        message = "Schade, deine Antwort war leider Falsch!"
```

<details>
<summary>Ganzer Code</summary>
<pre>

```python
from flask import Flask, render_template, jsonify, request
from random import randint

app = Flask(__name__)

@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(51, 100)
    return render_template("calc.html", a=a, b=b)

@app.route("/check-results")
def check_results():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    user_res = request.args.get('result', 0, type=int)
    if user_res == a + b:
        message = "Gratulation, deine Antwort ist richtig!"
    else:
        message = "Schade, deine Antwort war leider Falsch!"
```

</pre>
</details>

Nun müssen wir aber noch besagte message als dictonary verpacken, zum verständnis nennen wir sie jetzt `output`:

```python
    output = {"message":message}
```

Als letztes lassen wir uns mit Flasks `jsonify` das dictonary zu Json parsen (eng. ~übersetzten) und danach als für javascript lesbare Antwort zurückgeben:

```python
    return jsonify(output)
```

Und niemals zu vergessen ein `app.run()` als letztes hinten heranzu hängen:

<details>
<summary>Ganzer Code</summary>
<pre>

```python
from flask import Flask, render_template, jsonify, request
from random import randint

app = Flask(__name__)

@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(51, 100)
    return render_template("calc.html", a=a, b=b)

@app.route("/check-results")
def check_results():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    user_res = request.args.get('result', 0, type=int)
    if user_res == a+b:
        message = "Gratulation, deine Antwort ist richtig!"
    else:
        message = "Schade, deine Antwort war leider Falsch!"
    output = {"message": message}
    return jsonify(output)

app.run()
```

</pre>
</details>

Jetzt haben wir das nötigtste fertig und ihr könnt eure app.py ausführen:

Unter Linux:
`python3 app.py`

Einmal hier alles komplett:

<details>
<summary>Ordner Struktur</summary>
<pre>

```
Balmung
| app.py
│
└───templates
|   | calc.html
```

</pre>
</details>

<details>
<summary>Alle Datein</summary>
<pre>

<details>
<summary>app.py</summary>
<pre>

```python
from flask import Flask, render_template, jsonify, request
from random import randint

app = Flask(__name__)

@app.route("/")
def index():
    a = randint(1, 50)
    b = randint(51, 100)
    return render_template("calc.html", a=a, b=b)

@app.route("/check-results")
def check_results():
    a = request.args.get('a', 0, type=int)
    b = request.args.get('b', 0, type=int)
    user_res = request.args.get('result', 0, type=int)
    if user_res == a+b:
        message = "Gratulation, deine Antwort ist richtig!"
    else:
        message = "Schade, deine Antwort war leider Falsch!"
    output = {"message": message}
    return jsonify(output)

app.run()
```

</pre>
</details>

<details>
<summary>calc.html</summary>
<pre>

```html
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <title>Kopfrechentrainer</title>
    </head>
    <body>
        <script type="text/javascript">
            $(function () {
                $("form").submit(function (event) {
                    var url = "/check-results";
                    var a = $("#a").val();
                    var b = $("#b").val();
                    var user_res = $("#result").val();
                    var data = {
                        a: a,
                        b: b,
                        result: user_res
                    };
                    $.ajax({
                        dataType: "json",
                        url: url,
                        data: data,
                        success: function (data) {
                            $("#message").text(data.data.message)
                            }
                    });
                    event.preventDefault(event);
                });
            });
        </script>
        <form>
            <p>
                {{ a }} + {{ b }} =
                <input type="text" id="result">
                <input type="hidden" id="a" value="{{ a }}">
                <input type="hidden" id="b" value="{{ b }}">
                <input type="submit" value="Ergebnis absenden">
            </p>
        </form>
        <p id="message"></p>
    </body>
</html>
```

</pre>
</details>

</pre>
</details>

Tada! Ihr habt es geschafft eure erste App die asnychrone Abfragen an einen Server mit Javascript schikct und eine von Javascript lesbare Antwort mit Python.

<details>
<summary>Feinschliff für richtige Coder</summary>
<pre>

In diesem Feinschliff Abschnitt sorgen wir dafür, dass ein Knopf erscheint, sobald die Lösung abgesendet wurde, um die Seite neu zu laden. Dazu müssen wir eine neue Jquery Funktion kenenlernen und mit Javascript's hide/ show spielen.

Dieser Abschnitt ist noch in Arbeit aer bereits in dieser html Version enthalten.

```html
<!DOCTYPE html>
<html>
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <title>Kopfrechentrainer</title>
    </head>
    <body>
        <script type="text/javascript">
            $( document ).ready(function() {
                $("#refresh").hide();
            });
            $(function () {
                $("form").submit(function (event) {
                    var url = "/check-results";
                    var a = $("#a").val();
                    var b = $("#b").val();
                    var user_res = $("#result").val();
                    var data = {
                        a: a,
                        b: b,
                        result: user_res
                    };
                    $.ajax({
                        dataType: "json",
                        url: url,
                        data: data,
                        success: function (data) {
                            $("#message").text(data.data.message)
                            }
                    });
                    $("#refresh").show();
                    event.preventDefault(event);
                });
                $("#refresh").click(function () {
                    location.reload(true);
                });
            });
        </script>
        <form>
            <p>
                {{ a }} + {{ b }} =
                <input type="text" id="result">
                <input type="hidden" id="a" value="{{ a }}">
                <input type="hidden" id="b" value="{{ b }}">
                <input type="submit" value="Ergebnis absenden">
                <button type="button" id="refresh">Neue Übung</button>
            </p>
        </form>
        <p id="message"></p>
    </body>
</html>
```

</pre>
</details>

&copy; Chaostheorie 2019

[![CC-BY-NC](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cc-by-nc_icon.svg/128px-Cc-by-nc_icon.svg.png)](https://creativecommons.org/licenses/by-nc/4.0/)
