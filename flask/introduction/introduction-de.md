# Calculus
- - - 

Das Ziel ist eine Website auf der man zwei zufällige Zahlen addieren muss und darauf eine Antwort vom Server bekommt.
Die Voraussetzung zum Anfangen ist eine Installation von [Python 3]("https://python.org") und [Flask]("https://flask-user.readthedocs.io/en/latest/").

[Weiter](https://gitlab.com/Chaostheorie/balmung/tree/master/flask/introduction/first-page-de.md)

&copy; Chaostheorie 2019
[![CC-BY-NC](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cc-by-nc_icon.svg/128px-Cc-by-nc_icon.svg.png)](https://creativecommons.org/licenses/by-nc/4.0/)
