# CC-BY-NC 4.0 @Chaostheorie 2019 for more see https://gitlab.com/Chaostheorie/balmung

from flask import Flask, render_template_string

app = Flask(__name__)

@app.route("/")
def index():
    return render_template_string("<h4>Hallo Welt!</h4>")

app.run()