# Flask

## Erste Website

Am Anfang müssen wir eine Datei erstellen um unser Skript zu schreiben. 
Dies machen wir unter Linux (Debian/ Ubuntu) mit dem Shell Kommando `nano calc.py`.
Nun habt ihr einen Text Editor offen und könnt anfangen zu coden.
Wir fangen damit an die benötigten Module aus der Flask Biblothek zu importieren.
Dies tun wir mit

```python
from flask import Flask, render_template_string
```

Hiermit importieren wir die Hauptinstanz sowie eine Art die Websie zurückzugeben und eine Info über die Anfrage.
Als nächstes erstellen wir die Haupinstanz mit

```python
app = Flask(__name__)
```

damit wird die Variable `app` mit dieser Instanz verknüpft und gilt nun stellvertretend für sie.
Jetzt erstellen wir unsere Erste Funktion um etwas anzuzeigen.
Dafür deklarieren wir zuerest die Adresse, welche für die Seite genutzt werden soll. Hierfür müssen wir einen Decorator benutzten.

```python
@app.route("/")
def index():
   return render_template_string("<h4>Hallo Welt!</h4>")
```

als letztes müssen wir für unsere erste Website noch einen Start Punkt einbauen, dies tun wir mit:

```python
app.run()
```

Gleicht euren Code ruhig nocheinmal hiermit ab:

```python
from flask import Flask, render_template_string, request

app = Flask(__name__)

@app.route("/")
def index():
    return render_template_string("<h4>Hallo Welt</h4>")

app.run()
```

Nun können wir mit `Strg + O`  das Programm abspeichern und den Editor danach mit `Strg + X` beenden.
Jetzt können wir mit `python3 calc.py` das Progarmm starten.
daraufhin erhaltete ihr eine Ausgabe ähnlich wie diese:

```console
 * Serving Flask app "calc" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Nun könnt ihr über die Adresse nach `* Running on` die Website erreichen. In meinem Fall währe das `http://127.0.0.1:5000/`.
Nun seht ihr wahrscheinlich eine Seite mit dieser Nachricht:

<h4>Hallo Welt!</h4>

Und so schnell haben wir schon unsere erste Website mit Pythons Flask erstellt. Als nächstes könntet ihr euch unseren Kopfrechentrainer mit Pythons Flask "Calculus" ansehen.

[Hier gehts weiter zu Calculus](https://gitlab.com/Chaostheorie/balmung/tree/master/flask/calc/calc-de.md)

&copy; Chaostheorie 2019
[![CC-BY-NC](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cc-by-nc_icon.svg/128px-Cc-by-nc_icon.svg.png)](https://creativecommons.org/licenses/by-nc/4.0/)
