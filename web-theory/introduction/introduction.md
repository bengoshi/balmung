# Aufruf einer Website

## Einführung
  
In dieser Reihe lernst du was passiert, wenn du eine Website über deinen Browser aufrufst.
Was passiert eigentlich beim Aufruf einer Website?
Nachdem dem Öffnen des Browers geben wir in der Adressleiste eine Adresse (URL) oder einen Suchbgriff ein. Der einfachheitshalber gehen wir davon aus, dass du direkt eine URL wie http://dev.kobschaetzki.de eingibst. Wir werden direkt auf diese Seite geleitet und sehen sie dann.
Aber wie verbindet sich der Rechner übehaupt mit dem Internet?
Vorab eine Begriffserklärung: Ein Server ist ein Dienst, der darauf wartet, eine Anfrage eines sogenannten Clients zu beantworten.
Dein Computer ist hier der Client. Er benötigt zuerst eine Verbindung durch beispielsweise ein W-LAN zu einem Router oder direkt per LAN zum Router. Der Router übernimmt viel Arbeit für dich und sendet aber gleichzeitig auch alle deine Anfragen in das Internet hinaus. Was ein Router genau macht, kommt später dran. An dieser Stelle soll erstmal genügen, dass er Anfragen an die nächste Stelle weiterleitet.
Wir sind erstmal hier:

```mermaid
sequenceDiagram
    participant Client
    participant Router
    Client->>Router: HTTP Request
    Router->>Client: HTTP Response
```

Beim Request (engl. Anfrage), genauer HTTP-Request, fragt der Client beim Server nach einer Website an. In den Request schauen wir später genauer rein. Der Server antwortet mit einem HTTP-Response (engl. Antwort). Dort wird die eigentliche Internetseite, die du siehst, ausgeliefert. Bei HTTP handelt es sich um das HyperText Transfer Protocol. Es überträgt die Seiten als Text. Dein Browser wiederum hat eine sogenannte Renderingengine, die dafür sorgt, dass aus dem blanken übertragenen Text die Seite wird, die du nachher auch siehst.

Aber wir müssen nochmal einen Schritt zurückgehen, weil es in der Wirklich doch nicht so schnell geht. Du gibst Daraufhin verbindet sich der Router mit einem DNS (Domain Name Solver) Server und fragt nach der IP-Adresse für die Adresse. 
Nun sendet der Router diese Adresse wieder zurück an deinen Client.

```mermaid
sequenceDiagram
    participant Client
    participant Router
    participant DNS
    Router->>DNS: ? https://dev.kobschaetzki.de
    DNS->>Router: 213.73.114.206
    Router->>Client: HTTP Request
    Client->>Router: HTTP Response
```

Jetzt geht es erst richtig los da wir jetzt wissen woher wir unsere Daten holen müssen. Also fragt unser Router jetzt den Server unter der eben erhaltenen Adresse z.B. `213.73.114.206` und wartet auf eine Antwort.

```mermaid
sequenceDiagram
    participant Client
    participant Router
    participant DNS
    participant Server
    Router->>DNS: ? URL
    DNS->>Router: IP
    Client->>Router: HTTP Request
    Router->>Client: HTTP Response
    Router->>Server: HTTP Request
    Server->>Router: HTTP Response
```

Und nun haben wir unsere HTTP Response durch die HTTP Request bekommen und unser Browser kann die Website rendern.

[Hier ist unsere Tutorial was dahinter eigentlich passiert](https://gtlab.com/Chaostheorie/balmung/tree/master/web-theory/osi-model/introdution-de.md)

[Hier ist unser Tutorial wie du eine Website selber bauen kannst](https://gitlab.com/Chaostheorie/balmung/tree/master/flask/introduction/introduction-de.md)

&copy; Chaostheorie 2019
[![CC-BY-NC](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cc-by-nc_icon.svg/128px-Cc-by-nc_icon.svg.png)](https://creativecommons.org/licenses/by-nc/2.0/)
