# OSI-Model

## Die Schichten des Netzwerkverkehrs

Netzwerkverkehr wie z.B. ein Video von Youtube, das Aufrufen einer Website oder aber das Anhören von Musik auf Spotify läuft überall auf der Welt über das OSI-Model.
Diese Model besteht aus sieben Schichten. In diesem TEil unserer Reihe Webtheorie sehen wir uns diese Schichten an und gehen damit hinter die Kulissen normaler Anfragen im Internet.

Die sieben Schichten sind:
    1. [Bitübertragungsschicht](#1-bit%C3%BCbertragungsschicht--bituebertragungsschicht)
    2. [Sicherungsschicht](#2-sicherungsschicht-sicherungsschicht)
    3. [Vermittlungsebne](#3-vermittlungsebne--vermittlungsebne)
    4. [Transportebne](#4-transportebne--transportebne)
    5. [Sitzungsebne](#5-sitzungsebne--sitzungsebne)
    6. [Darstellungsebne](#6-darstellungsebne--darstellungsebne)

### 1. Bitübertragungsschicht {: #bituebertragungsschicht}

In der Bitübertragungsschicht auch physische Schicht genannt werden die Bits physisch übertragen. Hierzu zählen also auch Geräte wie eure Netzwerkkarte aber auch euer Kable oder ein im Antlantik liegendes Glasfaserkabel.
Diese Schicht enthält passend dazu auch Übertragungsmodelle wie das Übertragen von elektromagnetischen Wellen über z.B. Kupferkabel oder ähnliches um Daten zu übertragen.
Um dies zu bewerkstelligen sind bestimmte Komponenten immer benötigt, damit wir z.B. unser Video ansehen können:

* Ein Übertragungsmedium
  * Als Träger der Daten bei der Übertragung
* Ein Sender
  * Zum senden der Daten über das Medium
* Ein Empfänger
  * Zum Empfangen der Daten vom Medium

Normen: siehe [Bitübertragungsschicht](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_1_%E2%80%93_Bit%C3%BCbertragungsschicht_(Physical_Layer))

### 2. Sicherungsschicht {: #sicherungsschicht}

In dieser Schicht werden die Bits aus der ersten Schicht in Blöcke verarbeitet, deswegen trägt diese Schicht auch den anderen Namen Prozedurebne und sollte deswegen gut abgesichert sein.
Auf dieser Ebne werden weiterhn die Bits als Bitströme überprüft und fehlerhaftes kann mit z.B. Prüfsummen identifiziert werden und abgelehnt oder gar korregiert werden also z.B. ein Buchstabe einer aufegrufenen Website.
Als Hardware gehört hierzu meist der Teil eines Geräts der das Ethernetprotokoll verwendet also der Teil wo ihr das Übertragungsmedium an den Empfänger/ Sender anschließt.
Normen: siehe [Sicherungsschicht](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_2_%E2%80%93_Sicherungsschicht_(Data_Link_Layer))

### 3. Vermittlungsebne {: #vermittlungsebne}

In dieser Schicht werden die Pakete durch Dienste im Netzwerk verteilt und an die Verschieden Adressen die diese Schicht stellt gesendet.
Diese Schicht agiert Netzwerkweit und da nicht immer eine Direkte Verbindung zwischen Sender und Empfänger einer Übertragung möglich ist auch über zwischen Punkte wie z.B. Netzwerkknoten.
Neben einer IP-Adresse wie z.B. IPv4: 213.73.114.206 oder IPv6: 0:0:0:0:0:ffff:d549:72ce werden auch andere Adressen wie eine Netzwerkszugangsadresse (NSAP-Adress) vergeben.
Protokolle: IP, IPsec, ICMP, siehe [Normen](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_3_%E2%80%93_Vermittlungsschicht_(Network_Layer))

### 4. Transportebne {: #transportebne}

In diser Schicht werden Daten gebündelt und für die Schichten 5-7 bereitgestellt. Auch hierbei werden weitere Kontrollen durchgeführt und die Daten auf Fehler überprüft und notfalls korregiert oder verworfen.
Normen: siehe [Normen](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_4_%E2%80%93_Transportschicht_(Transport_Layer))

### 5. Sitzungsebne {: #sitzungsebne}

In deiser Schicht kommunizieren die Prozesse des Sender und Empfängers miteinander und mithilfe von Sitzungen u.Ä. wird das synchrone Übertragen- und Empfangen von Daten zwischen Systemen ermöglicht. Hier werden aber auch Techniken wie Fixpoints o. Checkpoints genutzt, welche es ermöglichen bei z.B. einem Ausfall der Verbindung die Sitzung und die damit verbundene Prozesse fortzusetzten also z.B. wenn dein W-LAN die Verbindug verliert und deine Musik stopt und nach wiederaufnehmen der Verbindung sich an der gleichen Stelle fortsetzt.
Normen: siehe [Normen](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_5_%E2%80%93_Sitzungsschicht_(Session_Layer))

### 6. Darstellungsebne {: darstellungsebne}

In dieser Schicht erfolgt, wie der Name schon vermuten lässt, die systematische Darstellung der Daten in einer unabhängigen Form zum Austausch.
Auch ist auf dieser Ebne die Verschlüsselung z.B. TLS und Kodierung z.B. Unicode angesiedelt. Gleichzeitig kann diese Ebne auch zwischen verschieden Kodierung bze. Darstellungsarten "übersetzten" und ermöglich es z.B. einem Rechner aus dem arabischen Raum mit dem Text der im Unicode kodiert wurde umgehen.
Protokolle und Normen: siehe [Protokolle und Normen](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_6_%E2%80%93_Darstellungsschicht_(Presentation_Layer))

### 7. Anwendungsschicht {: anwendungsschicht}

Dies ist die Schicht auf der wir Entwickler uns ammeisten bewegen und auf der Anwendungen hauptsächlich kommunizieren. Dies passiert z.B. über das HTTP Protokoll bei Web Anwendungen oder aber über das SecureShell-Protokoll beim Fernugriff per Shell.
Diese Schicht ermöglich es auch selber Protokolle mit einem vergleichsweise geringen Aufwand zu coden und gleichzeitig trägt sie nahezu alle für uns wichtigen Protokolle.
Protokolle und Normen: siehe [Protokolle und Normen](https://de.wikipedia.org/wiki/OSI-Modell#Schicht_7_%E2%80%93_Anwendungsschicht_(Application_Layer))

Quelle: [Wikipedia (de)](https://de.wikipedia.org/wiki/OSI-Modell)
&copy; Chaostheorie 2019
[![CC-BY-NC](https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Cc-by-nc_icon.svg/128px-Cc-by-nc_icon.svg.png)](https://creativecommons.org/licenses/by-nc/2.0/)
